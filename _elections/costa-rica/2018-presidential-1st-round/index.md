---
layout: election
title: "2018 Costa Rican presidential election - 1st round"
country: Costa Rica
country_code: cr
date: 2018-02-04
wikipedia: https://en.wikipedia.org/wiki/2018_Costa_Rican_general_election
source: http://resultados2018.tse.go.cr/resultadosdefinitivos/#/presidenciales
maxpercentage: 50
handicap: 2
race: both
candidates:
- name: Fabricio Alvarado
  name_short: PREN
  party: PREN
  color: "#FFD700"
- name: Carlos Alvarado
  name_short: PAC
  party: PAC
  color: "#0059CF"
- name: Antonio Álvarez
  name_short: PLN
  party: PLN
  color: "#008024"
- name: Rodolfo Piza
  name_short: PUSC
  party: PUSC
  color: "#0000FF"
- name: Juan Diego Castro
  name_short: PIN
  party: PIN
  color: "#122562"
- name: Rodolfo Hernández
  name_short: PRSC
  party: PRSC
  color: "#FF0000"
---
