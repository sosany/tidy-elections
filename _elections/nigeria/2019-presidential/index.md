---
layout: election
title: "2019 Nigerian presidential election"
country: Nigeria
country_code: ng
date: 2019-02-23
wikipedia: https://en.wikipedia.org/wiki/2019_Nigerian_general_election
source: https://www.bbc.co.uk/news/resources/idt-f0b25208-4a1d-4068-a204-940cbe88d1d3
maxpercentage: 95
handicap: 1
race: both
candidates:
- name: Muhammadu Buhari
  name_short: Buhari
  party: All Progressives Congress
  color: "#006903"
- name: Atiku Abubakar
  name_short: Atiku
  party: People's Democratic Party
  color: "#87BEEB"
---
